﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour
{

    public static LineDrawer _current;

    public Transform player, target;
    bool drawPath;

    LineRenderer lneRend;

    public bool DrawPath
    {
        set { drawPath = value; }
    }

    void Awake()
    {
        _current = this;
    }

    void Start()
    {
        lneRend = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        if (drawPath == true)
        {
            //lneRend.SetPosition(0, player.position);
            lneRend.SetPosition(1, target.position);
        }


    }

    public void ToggleLineActive(bool val)
    {
        if (val == false)
        {
            lneRend.positionCount = 0;
        }
        else
        {
            lneRend.positionCount = 2;
            lneRend.SetPosition(0, player.position);
            lneRend.SetPosition(1, target.position);
        }
        lneRend.enabled = val == true ? true : false;

    }

}
