﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMover : MonoBehaviour
{

    public float moveSpeed;
    public float screenWidth;
    Transform targetTransform;
    GameObject targetCentre;
    bool launch = false;

    float moveSpeedMultiplyer = 1.0f;

    public bool Launch
    {
        set { launch = value; ToggleCentreSprite(value); }
    }

    public float MoveSpeedMultiplyer
    {
        set { moveSpeedMultiplyer = value; }
    }

    void Awake()
    {
        screenWidth = Camera.main.orthographicSize * Camera.main.aspect;
    }

    private void Start()
    {
        moveSpeedMultiplyer = GameManager._current.SpeedMultiplyer;
        targetTransform = this.transform;
        if(targetCentre == null)
        {
            targetCentre = gameObject.transform.GetChild(0).gameObject;
        }
        targetCentre.SetActive(false);
    }

    void ToggleCentreSprite(bool active)
    {       

        if (targetCentre == null)
        {
            targetCentre = gameObject.transform.GetChild(0).gameObject;
        }

        targetCentre.SetActive(active);
    }

    //private void OnEnable()
    //{
    //    moveSpeedMultiplyer = GameManager._current.SpeedMultiplyer;
    //    //if (CameraMover.Current != null)
    //    //{
    //    //    CameraMover.Current.ChaseTarget = true;
    //    //    CameraMover.Current.ChaseSpeed = moveSpeed / 1.75f * moveSpeedMultiplyer;
    //    //}
    //    targetTransform = this.transform;
    //}

    public void UpdateCameraSpeed()
    {
        CameraMover.Current.TargetTransform = this.transform;
        CameraMover.Current.ChaseTarget = true;
        //CameraMover.Current.ChaseSpeed = moveSpeed / 1.75f * moveSpeedMultiplyer;
        CameraMover.Current.ChaseSpeed = moveSpeed;
    }

    private void OnDisable()
    {
        CameraMover.Current.ChaseTarget = false;
    }


    void Update()
    {
        if(launch == false)
        {
            return;
        }

        if (targetTransform.position.x < -screenWidth || targetTransform.position.x > screenWidth)
        {
            GameManager._current.TriggerPlayerDeath();

            //Vector3 newPos = transform.position;
            //if (newPos.x < 0)
            //{
            //    newPos.x = screenWidth;
            //}
            //else
            //{
            //    newPos.x = -screenWidth;
            //}

            //transform.position = newPos;
        }
        if (targetTransform.position.y <= CameraMover.Current.transform.position.y - Camera.main.orthographicSize)
        {
            GameManager._current.TriggerPlayerDeath();
        }
        if(targetTransform.position.y >= CameraMover.Current.transform.position.y + Camera.main.orthographicSize)
        {
            GameManager._current.TriggerPlayerDeath();
        }

        targetTransform.Translate(Vector2.up * (moveSpeed * moveSpeedMultiplyer) * Time.deltaTime);

    }
}
