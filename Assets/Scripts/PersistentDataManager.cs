﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentDataManager : MonoBehaviour {

    static PersistentDataManager _current;

    public static PersistentDataManager Current
    {
        get { return _current; }
    }

    public GameObject blockPrefab;
    public int blocksPerRow = 10;

    float audioVolume = 0.6f;

    Vector3 sessionBlockScale;

    public int BlocksPerRow
    {
        get { return blocksPerRow; }
        set { blocksPerRow = value; }
    }

    public float AudioVolume
    {
        get { return audioVolume; }
        set { audioVolume = value; }
    }

    public Vector3 SessionBlockScale
    {
        get { return sessionBlockScale; }
    }

    private void Awake()
    {
        if (_current == null)
        {
            _current = this;
            DontDestroyOnLoad(gameObject);
        }
        else if(_current != this)
        {
            Destroy(gameObject);
        }
    }

    //private void Start()
    //{
    //    SetBlockSize();
    //}

    internal Vector3 SetBlockSize()
    {
        SpriteRenderer rnd = blockPrefab.GetComponent<SpriteRenderer>() as SpriteRenderer;

        float worldScreenWidth = (Camera.main.orthographicSize * 2) / Screen.height * Screen.width;
        float newScale = (worldScreenWidth / rnd.sprite.bounds.size.x) / blocksPerRow;

        sessionBlockScale = new Vector3(newScale, newScale, 1.0f);
        return sessionBlockScale;        
    }
}
