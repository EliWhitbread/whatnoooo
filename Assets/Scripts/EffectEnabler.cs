﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectEnabler : MonoBehaviour
{
    ParticleSystem pSystem;
    bool isReady = true;
    [SerializeField] float effectLifetime = 3.0f;

    public bool IsReady
    {
        get { return isReady; }
    }

    public void Initialise()
    {
        if(null == pSystem)
        {
            pSystem = gameObject.GetComponent<ParticleSystem>();
        }
        pSystem.Stop();
        gameObject.transform.position = Vector2.zero * 20;
        isReady = true;
    }

    public void Emit()
    {
        if (null == pSystem)
        {
            pSystem = gameObject.GetComponent<ParticleSystem>();
        }
        pSystem.Play();
        isReady = false;

        StartCoroutine(Sleep());
    }

    IEnumerator Sleep()
    {
        yield return new WaitForSeconds(effectLifetime);
        pSystem.Stop();
        gameObject.transform.position = Vector2.zero * 20;
        isReady = true;
    }

    public void ForceStop()
    {
        pSystem.Stop();
        pSystem.Clear();
        gameObject.transform.position = Vector2.zero * 20;
        isReady = true;
    }
}
