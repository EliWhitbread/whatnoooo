﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public delegate void PrimaryActionDown();
    public static event PrimaryActionDown OnPrimaryActionDown;

    //public delegate void PrimaryActionUp();
    //public static event PrimaryActionUp OnPrimaryActionUp;


    bool touchDetected = false;
	
	void Update () {

#if UNITY_IOS || UNITY_ANDROID

        if(Input.touchCount > 0 && touchDetected == false)
        {
            if (OnPrimaryActionDown != null)
            {
                OnPrimaryActionDown();
            }
            touchDetected = true;
        }
        if(Input.touchCount < 1)
        {
            touchDetected = false;
        }

#endif
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (OnPrimaryActionDown != null)
            {
                OnPrimaryActionDown();
            }
        }
#endif
    }
}
