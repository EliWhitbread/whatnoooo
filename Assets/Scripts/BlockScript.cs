﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockScript : MonoBehaviour
{

    float spriteSize;
    bool resetSprite = false;
    SpriteRenderer myRenderer;

    public float SpriteSize
    {
        get { return spriteSize; }
    }

    public bool ResetSprite
    {
        get { return resetSprite; }
        set { resetSprite = value; }
    }

    private void Awake()
    {
        myRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    void OnEnable()
    {
        if(myRenderer == null)
        {
            myRenderer = gameObject.GetComponent<SpriteRenderer>();
        }
        myRenderer.color = Spawner.Current.BlockColour;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("BlocksOutOfBounds"))
        {
            gameObject.SetActive(false);
        }
    }

    //public void CheckSpriteState(float newSize, Color clr)
    //{

    //    SpriteRenderer rnd = GetComponent<SpriteRenderer>() as SpriteRenderer;
    //    rnd.color = clr;

    //    Sprite newSprite = Sprite.Create(Spawner.Current.masterTexture, new Rect(0.0f, 0.0f, newSize * 100, newSize * 100), new Vector2(0.5f, 0.5f), 100);

    //    rnd.sprite = newSprite;

    //    BoxCollider2D col = GetComponent<BoxCollider2D>();
    //    if (col == null)
    //    {
    //        col = gameObject.AddComponent<BoxCollider2D>();
    //        GetComponent<BoxCollider2D>().isTrigger = true;
    //    }

    //    col.size = rnd.bounds.size;
    //    spriteSize = newSize;
    //}

}
