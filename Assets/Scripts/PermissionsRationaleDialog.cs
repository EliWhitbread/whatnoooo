﻿using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class PermissionsRationaleDialog : MonoBehaviour
{
    int kDialogWidth = 300;
    int kDialogHeight = 100;
    bool windowOpen = true;

    void DoMyWindow(int windowID)
    {
        GUI.Label(new Rect(10, 20, kDialogWidth, kDialogHeight / 2), "Alow access to device storage?");
        GUI.Button(new Rect(10, kDialogHeight - 30, 300, 300), "No");
        if(GUI.Button(new Rect(kDialogWidth - 110, kDialogHeight - 30, 300, 300), "Yes"))
        {
#if PLATFORM_ANDROID
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
#endif
            windowOpen = false;
        }
    }

    private void OnGUI()
    {
        kDialogWidth = Screen.width / 2;
        kDialogHeight = kDialogWidth / 2;

        if(windowOpen == true)
        {
            Rect rect = new Rect((Screen.width / 2) - (kDialogWidth / 2), (Screen.height / 2) - (kDialogHeight / 2), kDialogWidth, kDialogHeight);
            GUI.ModalWindow(0, rect, DoMyWindow, "Permissions Request Dialog");
        }
    }
}
