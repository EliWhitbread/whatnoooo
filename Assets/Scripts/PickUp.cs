﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

    public EnumManager.PickupTypes myType = EnumManager.PickupTypes.Crystal;

    Animator anim;
    Transform puTransform;
    //GameObject pEmitter;
    int pickupScoreBonus = 0;
    float puCollectedScale = 0.1f;
    Vector3 playerPos = Vector3.zero, initialLocalScale = Vector3.one;

    public EnumManager.PickupTypes MyType
    {
        get { return myType; }
        set { myType = value; }
    }

    public int PickupScoreBonus
    {
        get { return pickupScoreBonus; }
        set { pickupScoreBonus = value; }
    }

    private void Awake()
    {
        Initialise();
    }

    void Initialise()
    {
        anim = gameObject.GetComponent<Animator>();
        puTransform = this.transform;
        //pEmitter = this.gameObject.transform.GetChild(0).gameObject;
        //pEmitter.SetActive(false);
    }

    private void OnEnable()
    {
        if(anim == null)
        {
            Initialise();
        }

        initialLocalScale = puTransform.localScale;

        switch (myType)
        {
            case EnumManager.PickupTypes.Crystal:
                anim.SetInteger("PickupID", 1);
                //pEmitter.SetActive(true);
                break;
            case EnumManager.PickupTypes.Shield:
                anim.SetInteger("PickupID", 2);
                break;
            case EnumManager.PickupTypes.ScoreBonus:
                anim.SetInteger("PickupID", 3);
                break;
            default:
                break;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("BlocksOutOfBounds"))
        {
            gameObject.SetActive(false);
        }
    }
    
    public void PickupCollected(Vector3 target)
    {
        playerPos = target;
        StartCoroutine("ScalePickup");
    }

    IEnumerator ScalePickup()
    {
        float _scale = 1.0f;

        while (_scale > puCollectedScale)
        {
            puTransform.localScale = initialLocalScale * _scale;
            puTransform.position = Vector3.MoveTowards(puTransform.position, playerPos, Time.deltaTime);
            _scale -= Time.deltaTime * 3.0f;
            yield return null;
        }

        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        StopCoroutine("ScalePickup");
    }
}
