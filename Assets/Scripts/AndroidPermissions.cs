﻿using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif
using UnityEngine.UI;

public class AndroidPermissions : MonoBehaviour
{
    //GameObject dialog = null;
    public GameObject permissionsCanvas;

    private void Start()
    {
        //permissionsCanvas.SetActive(true);
#if PLATFORM_ANDROID
        if(!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
            //dialog = new GameObject();
        }
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageRead);
            //dialog = new GameObject();
        }
#endif
    }

    //public void ButtonSelected(bool allow)
    //{
    //    if(true == allow)
    //    {
    //        Permission.RequestUserPermission(Permission.ExternalStorageWrite);
    //        Permission.RequestUserPermission(Permission.ExternalStorageRead);
    //    }

    //    permissionsCanvas.SetActive(false);
    //}
//    private void OnGUI()
//    {
//#if PLATFORM_ANDROID
//        if(!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
//        {
//            dialog.AddComponent<PermissionsRationaleDialog>();
//            return;
//        }
//        else if(dialog != null)
//        {
//            Destroy(dialog);
//        }
//#endif
//    }
}
