﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum e_EffectType { explosion };

public class EffectSpawner : MonoBehaviour
{

    static EffectSpawner current;

    public static EffectSpawner Current
    {
        get { return current; }
    }

    public EffectEnabler explosionPrefab;
    public int explosionCollectionPoolSize;

    List<EffectEnabler> explosionCollection;

    private void Awake()
    {
        if (current == null)
        {
            current = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        explosionCollection = new List<EffectEnabler>();

        for (int i = 0; i < explosionCollectionPoolSize; i++)
        {
            EffectEnabler exp = Instantiate(explosionPrefab) as EffectEnabler;
            exp.Initialise();
            exp.transform.position = Vector2.right * 20;
            explosionCollection.Add(exp);
        }
    }

    public void SpawnEffect(e_EffectType t, Vector2 pos)
    {
        switch (t)
        {
            case e_EffectType.explosion:
                SpawnExplosion(pos);
                break;
            default:
                break;
        }
    }

    void SpawnExplosion(Vector2 spnPos)
    {
        for (int i = 0; i < explosionCollection.Count; i++)
        {
            if (explosionCollection[i].IsReady)
            {
                explosionCollection[i].transform.position = spnPos;
                explosionCollection[i].transform.rotation = Quaternion.identity;
                explosionCollection[i].Emit();
                return;
            }
        }
    }

    public void ClearAll()
    {
        StartCoroutine(ClearActiveEffects());
    }

    IEnumerator ClearActiveEffects()
    {
        for (int i = 0; i < explosionCollection.Count; i++)
        {
            if (!explosionCollection[i].IsReady)
            {
                explosionCollection[i].ForceStop();
            }
        }

        yield return null;
    }

}
