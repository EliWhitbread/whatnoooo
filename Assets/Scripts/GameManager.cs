﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{

    public static GameManager _current;
    public GameObject gameOverText, inGameHud_Static, inGameHud_Dynamic;
    public TextMeshProUGUI ScoreText, TimeText, crystalText, roundScoreText, roundTimeAliveText, roundCrystalText, bestScoreText, continueWithCrystalsText;
    public float speedIncrement, pickupSpawnDelay, speedMultiplyer = 1.0f;


    GameObject player;
    PlayerMover myPlayer;
    MenuManager menuManager;
    AdMobManager adManager;

    int score, crystals, bestScore, crystalsToContinue = 6, levelCrystals = 0;
    float timeAlive, countTime, pickupSpawnTime;
    bool playerAlive, isMoving = false, roundActive, keepSessionStats = false;

    public bool IsMoving
    {
        set { isMoving = value; }
    }

    public bool PlayerAlive
    {
        get { return playerAlive; }
        set { playerAlive = value; }
    }

    public Transform Player
    {
        get { return player.transform; }
    }

    public float SpeedMultiplyer
    {
        get { return speedMultiplyer; }
    }

    public int Crystals
    {
        get { return crystals; }
        set { crystals += value; crystalText.text = crystals.ToString(); }
    }

    public int LevelCrystals
    {
        set { levelCrystals += value; }
    }

    public bool RoundActive
    {
        set { roundActive = value; }
    }
    
    void Awake()
    {
        _current = this;
        gameOverText.SetActive(false);
        inGameHud_Static.SetActive(true);
        inGameHud_Dynamic.SetActive(true);
        keepSessionStats = false;
        ResetStats();
        playerAlive = true;
        roundActive = true;
    }

    void Start()
    {
        InputManager.OnPrimaryActionDown += InputManager_OnPrimaryActionDown;
        GetPlayerReference();
        PlayerSaveData pData = GetPersistantData();
        bestScore = pData.bestScore;
        UpdateBestScore(bestScore);
        Crystals = pData.crystals;
        crystalText.text = crystals.ToString();
        menuManager = gameObject.GetComponent<MenuManager>();
        if(menuManager == null)
        {
            menuManager = gameObject.AddComponent<MenuManager>();
        }
        if(adManager == null)
        {
            adManager = GameObject.FindGameObjectWithTag("AdsManager").GetComponent<AdMobManager>();
        }
        pickupSpawnDelay = (float)PersistentDataManager.Current.BlocksPerRow;
        pickupSpawnDelay = Mathf.Clamp(pickupSpawnDelay, 5.0f, 10.0f);
        levelCrystals = 0;
    }

    void GetPlayerReference()
    {
        if (myPlayer == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            if(player == null)
            {
                player = Instantiate(Resources.Load("Player", typeof(GameObject)) as GameObject,
                    new Vector3(0.0f, -1.46f, 0.0f), Quaternion.identity);
            }
            myPlayer = player.GetComponent<PlayerMover>();
        }
    }

    private void InputManager_OnPrimaryActionDown()
    {
        if (playerAlive == true && roundActive == true)
        {
            myPlayer.Launch();
        }
    }

    void Update()
    {
        if (playerAlive == true && isMoving == true)
        {
            //float t = Time.deltaTime;

            timeAlive += Time.deltaTime;
            TimeText.text = Mathf.Floor(timeAlive / 60).ToString("00") + ":" + Mathf.Floor(timeAlive % 60).ToString("00");

            pickupSpawnTime += Time.deltaTime;
            if (pickupSpawnTime >= pickupSpawnDelay)
            {
                if (Spawner.Current.AvailablePickups <= 3)
                {
                    Spawner.Current.AvailablePickups = 1;
                }
                
                pickupSpawnTime = 0;
            }

        }
    }

    public void UpdateScore(int amt)
    {
        if(player.activeSelf == false)
        {
            return;
        }

        Spawner.Current.SpawnPopupText(player.transform.position + new Vector3(0.0f, Random.Range(0.2f, 0.5f), 0.0f), amt);
        score += amt;
        ScoreText.text = score.ToString();
        if(bestScore < score)
        {
            bestScore = score;
            UpdateBestScore(bestScore);
        }
    }

    void UpdateBestScore(int amt)
    {
        bestScoreText.text = amt.ToString();
    }

    void ResetStats()
    {
        if(keepSessionStats == false)
        {
            score = 0;
        }
        
        timeAlive = 0;
        ScoreText.text = score.ToString();
        //TimeText.text = Mathf.Floor(timeAlive / 60).ToString("00") + ":" + Mathf.Floor(timeAlive % 60).ToString("00") + "." + Mathf.Floor((timeAlive * 1000) % 60).ToString("00");
        TimeText.text = Mathf.Floor(timeAlive / 60).ToString("00") + ":" + Mathf.Floor(timeAlive % 60).ToString("00");
    }

    public void RoundOver()
    {
        crystalsToContinue = crystals / 4;
        crystalsToContinue = (int)Mathf.Clamp(crystalsToContinue, 10, 100);

        roundActive = false;
        roundCrystalText.text = crystals.ToString();
        //roundScoreText.text = score.ToString();
        if(levelCrystals > 0)
        {
            roundScoreText.SetText("{0} + {1} <sprite=0>", score, levelCrystals);
        }
        else
        {
            roundScoreText.SetText("{0}", score);
        }
        roundTimeAliveText.text = Mathf.Floor(timeAlive / 60).ToString("00") + ":" + Mathf.Floor(timeAlive % 60).ToString("00") + "." + Mathf.Floor((timeAlive * 1000) % 60).ToString("00");
        continueWithCrystalsText.SetText("Spend {0} Whatsits <sprite=0> to CONTINUE", crystalsToContinue);
        if(crystals >= crystalsToContinue)
        {
            continueWithCrystalsText.alpha = 1.0f;
        }
        else
        {
            continueWithCrystalsText.alpha = 0.2f;
        }
        //inGameHud_Static.SetActive(false);
        //inGameHud_Dynamic.SetActive(false);
        //gameOverText.SetActive(true);
        UpdatePersistantData();
        StartCoroutine(ShowDeathCanvas());
    }

    IEnumerator ShowDeathCanvas()
    {
        yield return new WaitForSeconds(1.5f);
        inGameHud_Static.SetActive(false);
        inGameHud_Dynamic.SetActive(false);
        gameOverText.SetActive(true);
        
        roundScoreText.SetText("{0} + {1} <sprite=0>", score, levelCrystals);

        int _addScore = 500 * PersistentDataManager.Current.BlocksPerRow;
        float _tick = 3.0f / levelCrystals;
        if (_tick > 0.2f)
        {
            _tick = 0.2f;
        }

        while (levelCrystals > 0)
        {
            yield return new WaitForSeconds(_tick);
            score += _addScore;
            levelCrystals--;
            roundScoreText.SetText("{0} + {1} <sprite=0>", score, levelCrystals);
        }

        roundScoreText.SetText("{0} + {1} <sprite=0>", score, levelCrystals);

        roundScoreText.SetText("{0}", score);
    }

    public void RespawnPlayer()
    {
        if (levelCrystals > 0)
        {
            score += (500 * PersistentDataManager.Current.BlocksPerRow) * levelCrystals;
        }
        UpdatePersistantData();
        ResetStats();
        speedMultiplyer = 1.0f;
        gameOverText.SetActive(false);
        inGameHud_Static.SetActive(true);
        inGameHud_Dynamic.SetActive(true);
        //roundTimeAliveText.text = "";
        //roundScoreText.text = "";
        //roundCrystalText.text = "";
        playerAlive = true;
        isMoving = false;
        keepSessionStats = false;
        levelCrystals = 0;
    }

    void UpdatePersistantData()
    {
        PlayerSaveData tmpData = JsonData.instance.LoadSavaData();
        if(score > tmpData.bestScore)
        {
            tmpData.bestScore = score;
        }
        tmpData.crystals = crystals;
        if(timeAlive > tmpData.bestTime)
        {
            tmpData.bestTime = timeAlive;
        }

        JsonData.instance.SavePlayerData(tmpData);
    }

    PlayerSaveData GetPersistantData()
    {
        return JsonData.instance.LoadSavaData();
    }

    public void UpdateSpeedMultiplyer()
    {
        speedMultiplyer += speedIncrement;
        CameraMover.Current.SpeedMultiplyer = speedMultiplyer;
    }

    public void TriggerPlayerDeath()
    {
        playerAlive = false;
        myPlayer.Launch();
        myPlayer.TriggerPlayerDeath();
    }

    public void ShowRewardAd()
    {
        adManager.ShowRewardAd();
    }

    public void AdFailed()
    {
        keepSessionStats = false;
        menuManager.SwitchScene(0);
    }

    public void ContinueAfterAd(bool rewardPlayer = true)
    {
        if(rewardPlayer)
        {
            keepSessionStats = true;
            crystals += 10;
            crystalText.text = crystals.ToString();
            myPlayer.RespawnPlayer();
            
            return;
        }

        keepSessionStats = false;
        menuManager.SwitchScene(0);
    }

    public void ContinueWithCrystals()
    {
        if(crystals >= crystalsToContinue)
        {
            keepSessionStats = true;
            crystals -= crystalsToContinue;
            crystalText.text = crystals.ToString();
            myPlayer.RespawnPlayer();
        }
    }

    void OnDestroy()
    {
        InputManager.OnPrimaryActionDown -= InputManager_OnPrimaryActionDown;
    }

}
