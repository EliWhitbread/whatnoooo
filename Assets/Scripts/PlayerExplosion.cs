﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerExplosion : MonoBehaviour
{
    ParticleSystem pSystem;

    private void Awake()
    {
        if(pSystem == null)
        {
            pSystem = gameObject.GetComponent<ParticleSystem>();
        }
        pSystem.Stop();
    }

    public void ShowEffect(Vector3 pos)
    {
        gameObject.transform.position = pos;

        if (pSystem == null)
        {
            pSystem = gameObject.GetComponent<ParticleSystem>();
        }

        pSystem.Play();
    }

}
