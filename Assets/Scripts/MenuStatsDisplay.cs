﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class MenuStatsDisplay : MonoBehaviour
{
    public TextMeshProUGUI statsHighScore;
    [SerializeField] GameObject clearDataPanel;
    
    void Start()
    {
        statsHighScore.text = JsonData.instance.LoadSavaData().bestScore.ToString();
    }
    
    public void ResetStatsPanel(bool show = false)
    {
        clearDataPanel.SetActive(show);
    }

    public void ClearData(bool allData = false)
    {
        if(clearDataPanel.activeInHierarchy)
        {
            JsonData.instance.ClearSavedData(allData);
            statsHighScore.text = JsonData.instance.LoadSavaData().bestScore.ToString();
        }
        clearDataPanel.SetActive(false);
    }
}
