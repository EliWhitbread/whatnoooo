﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
////using UnityEngine.Monetization;
//using UnityEngine.Advertisements;

//public class AdsManager : MonoBehaviour, IUnityAdsListener
//{

//    public static AdsManager _current { get; private set; }
//    string placementID = "", gameID = "";
//    bool testMode = true, adReady = false;

//    private void Awake()
//    {
//        if (_current == null)
//        {
//            _current = this;
//        }
//        else
//        {
//            Destroy(this.gameObject);
//        }

//    }

//    private void Start()
//    {
//#if UNITY_ANDROID
//        gameID = "1421112";
//#elif UNITY_IOS
//        gameID = "1421113";
//#endif  
//        placementID = "rewardedVideo";

//        //Monetization.Initialize(gameID, testMode);
//        //Advertisement.AddListener(this);
//        if (Advertisement.isSupported)
//        {
//            Advertisement.AddListener(this);
//            Advertisement.Initialize(gameID, false);
//        }

//    }

//    public void ShowRewardAd()
//    {
//        //StartCoroutine(WaitForAd());
//        if (adReady == true)
//        {
//            Advertisement.Show(placementID);
//        }

//    }

//    //IEnumerator WaitForAd()
//    //{
//    //    if(!Monetization.IsReady(placementID))
//    //    {
//    //        yield return null;
//    //    }

//    //    ShowAdPlacementContent showAd = null;
//    //    showAd = Monetization.GetPlacementContent(placementID) as ShowAdPlacementContent;

//    //    if (showAd != null)
//    //    {
//    //        showAd.Show(AdCallbackHandler);
//    //    }
//    //}

//    //void AdCallbackHandler(ShowResult result)
//    //{
//    //    switch (result)
//    //    {
//    //        case ShowResult.Failed:
//    //            GameManager._current.ContinueAfterAd(false);
//    //            break;
//    //        case ShowResult.Skipped:
//    //            GameManager._current.ContinueAfterAd(false);
//    //            break;
//    //        case ShowResult.Finished:
//    //            GameManager._current.ContinueAfterAd(true);
//    //            break;
//    //        default:
//    //            break;
//    //    }
//    //}

//    public void OnUnityAdsReady(string _placementId)
//    {
//        if (_placementId == placementID)
//        {
//            adReady = true;
//            // Advertisement.Show(placementID);
//        }
//    }

//    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
//    {
//        switch (showResult)
//        {
//            case ShowResult.Failed:
//                GameManager._current.UpdateScore(7777);
//                GameManager._current.ContinueAfterAd(false);
//                break;
//            case ShowResult.Skipped:
//                GameManager._current.ContinueAfterAd(false);
//                GameManager._current.UpdateScore(9999);
//                break;
//            case ShowResult.Finished:
//                GameManager._current.ContinueAfterAd(true);
//                break;
//            default:
//                break;
//        }
//        adReady = false;
//    }

//    public void OnUnityAdsDidError(string message)
//    {
//        GameManager._current.UpdateScore(9000);
//        adReady = false;
//        Debug.Log("Ads failed to load");
//    }

//    public void OnUnityAdsDidStart(string placementId)
//    {
//        //throw new System.NotImplementedException();
//    }
//}
