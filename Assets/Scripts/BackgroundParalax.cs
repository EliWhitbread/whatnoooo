﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParalax : MonoBehaviour
{
    Transform myTransform;
    Material mat;
    Vector2 matOffset = Vector2.zero;
    float moveSpeed = 0.01f, moveSpeedMultiplyer = 0;

    public float MoveSpeedMultiplayer
    {
        set { moveSpeedMultiplyer = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        myTransform = gameObject.GetComponentInParent<Transform>();
        mat = gameObject.GetComponent<MeshRenderer>().material;
        matOffset.y = Random.Range(0.0f, 1.0f);
        mat.mainTextureOffset = matOffset;
    }

    // Update is called once per frame
    void Update()
    {
        matOffset.y += (moveSpeed * moveSpeedMultiplyer) * Time.deltaTime;
        mat.mainTextureOffset = matOffset;

    }
}
