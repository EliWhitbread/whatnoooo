﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMover : MonoBehaviour
{

    public float rotationSpeed;
    public GameObject target;
    public int jumpScore, bonusMultiplyer = 10;
    //public Text bonusText;
    public float minTargetDistForBonus;

    bool targetMoving, waitForPlayerMove = true, rotateLeft = true;
    int bonus, rotDirMultiplyer, difficultyBonus;
    float playerScale = 1.0f, camOrthoSize;
    public Vector2 spawnPosCamRelative;
    Transform playerTransform;
    TargetMover tMover;

    public float PlayerScale
    {
        set { playerScale = (value / 3) * 2; }
    }
    
    void OnEnable()
    {
        //transform.localScale = new Vector3(playerScale, playerScale, playerScale);
        playerTransform = this.transform;
        playerTransform.position = (Vector2)Camera.main.transform.position + spawnPosCamRelative;
        if(target == null)
        {
            target = GameObject.FindGameObjectWithTag("Target");
        }
        target.transform.position = playerTransform.position;
        target.transform.rotation = playerTransform.rotation;
        //target.SetActive(false);
        //GameManager._current.gameOverText.SetActive(false);
        playerTransform.rotation = Quaternion.AngleAxis(Random.Range(0.0f, 360.0f), Vector3.forward);
        rotDirMultiplyer = Random.Range(0, 2) == 0 ? -1 : 1;
        waitForPlayerMove = true;
        camOrthoSize = Camera.main.orthographicSize;
        tMover = target.GetComponent<TargetMover>();
        tMover.Launch = false;
        difficultyBonus = PersistentDataManager.Current.BlocksPerRow;
    }

    // Update is called once per frame
    void Update()
    {

        if (targetMoving == false)
        {
            playerTransform.Rotate(Vector3.forward * ((rotationSpeed * rotDirMultiplyer) * GameManager._current.SpeedMultiplyer) * Time.deltaTime);
        }
        else
        {
            float dist = Vector2.Distance(playerTransform.position, target.transform.position);
            if (dist >= minTargetDistForBonus)
            {
                bonus = Mathf.FloorToInt(dist * (bonusMultiplyer * difficultyBonus));
               // bonusText.text = "Bonus: " + bonus.ToString();
            }
            
            if(dist >= 0.5f)
            {
                Vector2 dir = target.transform.position - playerTransform.position;
                float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                angle -= 90.0f;
                playerTransform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0.0f, 0.0f, angle), 50.0f * Time.deltaTime);
            }

            
        }

        if (playerTransform.position.y <= CameraMover.Current.transform.position.y - camOrthoSize)
        {
            TriggerPlayerDeath();
        }

    }

    public void Launch()
    {
        if (waitForPlayerMove == true)
        {
            waitForPlayerMove = false;
            CameraMover.Current.CanMove = true;
            Spawner.Current.CanSpawn = true;
            GameManager._current.IsMoving = true;
        }

        if (targetMoving == false)
        {
            tMover.Launch = true;
            tMover.MoveSpeedMultiplyer = GameManager._current.SpeedMultiplyer;
            targetMoving = true;
            target.transform.position = playerTransform.position;
            target.transform.rotation = playerTransform.rotation;
            target.SetActive(true);
            AudioManager.curAudioManager.PlayEffect(AudioEffects.TARGET_MOVE);
            if (playerTransform.rotation.eulerAngles.z <= 70.0f || playerTransform.rotation.eulerAngles.z >= 290.0f)
            {
                tMover.UpdateCameraSpeed();
            }
        }
        else
        {
            tMover.Launch = false;
            targetMoving = false;
            playerTransform.position = target.transform.position;
            playerTransform.rotation = Quaternion.AngleAxis(Random.Range(0.0f, 360.0f), Vector3.forward);
            rotDirMultiplyer = Random.Range(0, 2) == 0 ? -1 : 1;
            target.SetActive(false);
            AudioManager.curAudioManager.PlayEffect(AudioEffects.WARP);
            if (gameObject.activeInHierarchy == true)
            {
                StartCoroutine(DelayBonusAddition());
            }
            target.SetActive(true);
        }
    }

    IEnumerator DelayBonusAddition()
    {
        yield return null;
        
        if (GameManager._current.PlayerAlive == true)
        {
            JumpSuccessful();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Pickup"))
        {
            AudioManager.curAudioManager.PlayEffect(AudioEffects.COLLECT_PICKUP);
            PickUp puScript = other.GetComponent<PickUp>();

            switch (puScript.MyType)
            {
                case EnumManager.PickupTypes.Crystal:
                    GameManager._current.Crystals = 1;
                    GameManager._current.LevelCrystals = 1;
                    break;
                case EnumManager.PickupTypes.Shield:
                    break;
                case EnumManager.PickupTypes.ScoreBonus:
                    GameManager._current.UpdateScore(puScript.PickupScoreBonus);
                    break;
                default:
                    break;
            }

            if(gameObject.activeSelf)
            {
                //StartCoroutine(DelayDeactivate(other.transform, 0.08f));
                puScript.PickupCollected(playerTransform.position);
            }
            
            
        }
        else
        {
            GameManager._current.TriggerPlayerDeath();
        }
        
    }

    void JumpSuccessful()
    {
        GameManager._current.UpdateScore(jumpScore * difficultyBonus);
        GameManager._current.UpdateScore(bonus);
    }

    public void TriggerPlayerDeath()
    {
        AudioManager.curAudioManager.PlayEffect(AudioEffects.DEATH);
        Spawner.Current.SpawnPlayerDeathEffect(playerTransform.position);
        StartCoroutine(PlayerDeathFadeOut());
        PlayerDied();
    }

    void PlayerDied()
    {
        //GameManager._current.PlayerAlive = false;
        GameManager._current.IsMoving = false;
        //GameManager._current.gameOverText.SetActive(true);
        GameManager._current.RoundOver();
        //gameObject.SetActive(false);
        target.SetActive(false);
        CameraMover.Current.CanMove = false;
        //LineDrawer._current.DrawPath = false;
        Spawner.Current.CanSpawn = false;
        //LineDrawer._current.ToggleLineActive(false);
    }

    IEnumerator PlayerDeathFadeOut()
    {
        Vector3 _scale = playerTransform.localScale;

        float _fadeTime = 1.0f;
        while (_fadeTime >= 0.1f)
        {
            playerTransform.localScale = _scale * _fadeTime;
            _fadeTime -= Time.deltaTime * 2.0f;
            yield return null;
        }

        playerTransform.localScale = _scale;

        gameObject.SetActive(false);
    }

    public void RespawnPlayer()
    {
        CameraMover.Current.ResetCamera();
        GameManager._current.RespawnPlayer();
        Spawner.Current.ClearAll();
        target.SetActive(false);
        CameraMover.Current.CanMove = false;
        waitForPlayerMove = true;
        playerTransform.position = (Vector2)Camera.main.transform.position + spawnPosCamRelative;
        target.transform.position = playerTransform.position;
        target.transform.rotation = playerTransform.rotation;
        targetMoving = false;
        gameObject.SetActive(true);
        //LineDrawer._current.ToggleLineActive(true);
        GameManager._current.RoundActive = true;
        target.SetActive(true);
        tMover.Launch = false;

    }

    //IEnumerator DelayDeactivate(Transform obj, float time)
    //{
    //    yield return new WaitForSeconds(time);
    //    obj.gameObject.SetActive(false);
    //}
}
