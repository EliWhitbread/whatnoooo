﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopupText : MonoBehaviour
{
    Transform popupTransform, camTransform;
    Vector3 endPos;
    TextMeshPro messageText;

    private void Awake()
    {
        popupTransform = this.transform;
        camTransform = Camera.main.transform;
        messageText = gameObject.GetComponent<TextMeshPro>();
        messageText.text = "";
    }

    public void Show(int amt)
    {
        messageText.text = "+" + amt.ToString();
        messageText.alpha = 1.0f;
        StartCoroutine(FadeText());
    }
    
    IEnumerator FadeText()
    {
        endPos = (Vector2)camTransform.position + (Vector2.up * 10.0f) + (Random.insideUnitCircle * 3.0f);

        float _t = Random.Range(0.8f, 1.2f);
        float _Speed = Random.Range(0.8f, 1.5f);
        while (_t >= 0.0f)
        {
            popupTransform.position = Vector3.MoveTowards(popupTransform.position, (Vector3)endPos, _Speed * Time.deltaTime);
            messageText.alpha -= Time.deltaTime;
            _t -= Time.deltaTime;
            yield return null;
        }

        gameObject.SetActive(false);
    }

}
