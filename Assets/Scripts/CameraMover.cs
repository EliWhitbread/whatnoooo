﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{

    static CameraMover _current;
    
    [SerializeField]float chaseSpeed, moveSpeed;
    [SerializeField]bool chaseTarget = false;

    [SerializeField]
    bool canMove = false;
    float refPosY, blockHeight, speedMultiplyer = 1.0f, screenHeight = 0.0f;
    Vector3 startPos, targetSpawnPos;
    Transform camTransform, targetTransform;
    Vector3 velocity = Vector3.zero;
    float newSpeed = 0;

    public static CameraMover Current
    {
        get { return _current;}
    }

    public float NewSpeed
    {
        get { return newSpeed; }
    }

    public float ChaseSpeed
    {
        get { return chaseSpeed; }
        set { chaseSpeed = value; }
    }

    public bool ChaseTarget
    {
        get { return chaseTarget; }
        set { chaseTarget = value;}
    }

    public Transform TargetTransform
    {
        get { return targetTransform; }
        set { targetTransform = value; targetSpawnPos = targetTransform.position; }
    }

    public float BlockHeight
    {
        set { blockHeight = value; }
    }

    public bool CanMove
    {
        get { return canMove; }
        set { canMove = value; }
    }

    public float SpeedMultiplyer
    {
        set { speedMultiplyer = value; }
    }

    void Awake()
    {
        _current = this;
        camTransform = this.transform;
        refPosY = camTransform.position.y;
        startPos = camTransform.position;
        //camTransform.GetComponent<Camera>().orthographicSize = (float)(Screen.currentResolution.height / 100.0f) * 0.5f;
    }

    void Start()
    {
        //blockHeight = Spawner.Current.blockSize;
        screenHeight = Camera.main.orthographicSize;

        Transform bounds = this.transform.Find("BlocksOutOfBounds").transform;
        Vector3 boundsPos = bounds.position;
        boundsPos.y = (this.transform.position.y - screenHeight) - blockHeight * 2;
        bounds.position = boundsPos;
    }

    void LateUpdate()
    {

        if (canMove == true)
        {
            if(chaseTarget == true)
            {
                camTransform.Translate(Vector2.up * ((moveSpeed * 1.1f) * speedMultiplyer) * Time.fixedDeltaTime);

                //float distToTarget = (camTransform.position.y + screenHeight) - targetTransform.position.y;

                //newSpeed = (distToTarget / 5.5f) <= (moveSpeed * speedMultiplyer) ? (moveSpeed * speedMultiplyer) : (distToTarget / 5.5f);

                //camTransform.Translate(Vector2.up * newSpeed * Time.fixedDeltaTime);

                //targetSpawnPos = targetTransform.position;
            }
            else
            {
                camTransform.Translate(Vector2.up * (moveSpeed * speedMultiplyer) * Time.fixedDeltaTime);
            }
            
        }

    }

    private void Update()
    {
        if (camTransform.position.y >= refPosY + blockHeight)
        {
            if ((Spawner.Current.YPos - camTransform.position.y) < 8.0f) //magic number... ewwwww! but is const.
            {
                Spawner.Current.SpawnRow(3);
            }
            else
            {
                Spawner.Current.SpawnRow(1);
            }
            refPosY = camTransform.position.y;
            
        }
    }

    public void ResetCamera()
    {
        camTransform.position = startPos;
        refPosY = camTransform.position.y;
    }
}
