﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectKiller : MonoBehaviour {

    public float effectDisableDelay;

    private void OnEnable()
    {
        Invoke("DisableEffect", effectDisableDelay);
    }

    void DisableEffect()
    {
        gameObject.SetActive(false);
    }
}
