﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    static Spawner _current;

    public static Spawner Current
    {
        get { return _current; }
    }

    public GameObject block, pickUp;
    public int poolSize, pickUpPoolSize, spawnRandomUpperLimit = 6, spritesPerRow, maxBlocksPerRow, rowsUntilNextLevel, pickupScoreBonus;
    public float maxSpawnDistX, spawnDistY;
    public Transform cam;

    [SerializeField]float blockSize, yPos = 0.0f;
    //public Color[] blockColours;

    //custom block
    public Texture2D masterTexture;
    Color blockColour;
    public Sprite newBLock;


    List<GameObject> blockCollection;
    float myTimer, screenWidth, spriteSize;
    int refBlocksPerRow, rowsSpawned, availablePickups = 0, spriteLevel = 0, initialSpawnRandomUpperLimit, initialRowsUntilNextLevel, initialSpriteWidth;
    bool canSpawn = false, canSpawnPickup = false;

    List<GameObject> pickupCollection;
    Vector3 spawnObjectScale = Vector3.one;

    //[SerializeField]GameObject masterDeathEffect;
    [SerializeField]PlayerExplosion deathEffect;

    //popup text
    List<PopupText> popupTextCollection;


    public bool CanSpawn
    {
        set { canSpawn = value; }
    }

    public int AvailablePickups
    {
        get { return availablePickups; }
        set { availablePickups += value; }
    }

    public int RowsSpawned
    {
        set { rowsSpawned = value; }
    }

    public float YPos
    {
        get { return yPos; }
    }

    public int SpawneRandomUpperLimit
    {
        get { return spawnRandomUpperLimit; }
        set { spawnRandomUpperLimit = value; }
    }

    public float SpriteSize
    {
        get { return spriteSize; }
    }

    public Color BlockColour
    {
        get { return blockColour; }
        set { blockColour = value; }
    }

    void Awake()
    {
        if (_current == null)
        {
            _current = this;
        }
        else
        {
            Destroy(gameObject);
        }

        if (spritesPerRow == 0)
        {
            spritesPerRow = 10;
        }

        blockColour = PickNewColour();
        SetBlockSize();

        blockCollection = new List<GameObject>();

        for (int i = 0; i < poolSize; i++)
        {
            GameObject obj = (GameObject)Instantiate(block);
            obj.SetActive(false);
            blockCollection.Add(obj);
        }

        pickupCollection = new List<GameObject>();

        for (int x = 0; x < pickUpPoolSize; x++)
        {
            GameObject pu = (GameObject)Instantiate(pickUp);
            pu.SetActive(false);
            pickupCollection.Add(pu);
        }

        popupTextCollection = new List<PopupText>();

        for (int p = 0; p < 2; p++)
        {
            PopupText _pt = Instantiate(Resources.Load("PopupText", typeof(PopupText)) as PopupText);
            _pt.transform.gameObject.SetActive(false);
            popupTextCollection.Add(_pt);
        }

        //CreateNewSprite();
        refBlocksPerRow = spritesPerRow;
        blockSize = block.GetComponent<Renderer>().bounds.size.x;
        CameraMover.Current.BlockHeight = blockSize;
        screenWidth = Camera.main.orthographicSize * Camera.main.aspect;
        rowsSpawned = 0;
        initialSpawnRandomUpperLimit = spawnRandomUpperLimit;
        initialRowsUntilNextLevel = rowsUntilNextLevel;

        //poolSize = (int)(spritesPerRow * 2 + ((Camera.main.orthographicSize * 2) * spritesPerRow));

        StartCoroutine(SpawnInitialGrid());
    }

    private void Start()
    {
        deathEffect = Instantiate(Resources.Load("DeathEffect", typeof(PlayerExplosion)), Vector3.right * 20, Quaternion.identity) as PlayerExplosion;
    }
    //void Start()
    //{

    //    blockCollection = new List<GameObject>();

    //    for (int i = 0; i < poolSize; i++)
    //    {
    //        GameObject obj = (GameObject)Instantiate(block);
    //        obj.SetActive(false);
    //        blockCollection.Add(obj);
    //    }

    //    pickupCollection = new List<GameObject>();

    //    for (int x = 0; x < pickUpPoolSize; x++)
    //    {
    //        GameObject pu = (GameObject)Instantiate(pickUp);
    //        pu.SetActive(false);
    //        pickupCollection.Add(pu);
    //    }
    //}

    //void SpawnBlock()
    //{
    //    Vector2 pos = GetSpawnPosition();

    //    for (int i = 0; i < blockCollection.Count; i++)
    //    {
    //        if (!blockCollection[i].activeInHierarchy)
    //        {
    //            blockCollection[i].transform.position = pos;
    //            blockCollection[i].transform.rotation = Quaternion.identity;
    //            blockCollection[i].SetActive(true);
    //            return;
    //        }
    //    }
    //}

    Vector2 GetSpawnPosition()
    {
        float posX = Random.Range(-maxSpawnDistX, maxSpawnDistX);
        Vector2 spawnPos = new Vector2(posX, cam.position.y + spawnDistY);

        return spawnPos;
    }

    public void ClearAll()
    {
        canSpawn = false;
        rowsSpawned = 0;
        spriteLevel = 0;
        yPos = 0.0f;
        spawnRandomUpperLimit = initialSpawnRandomUpperLimit;
        rowsUntilNextLevel = initialRowsUntilNextLevel;

        for (int i = 0; i < blockCollection.Count; i++)
        {
            if (blockCollection[i].activeInHierarchy)
            {
                blockCollection[i].SetActive(false);
            }
        }

        for (int x = 0; x < pickupCollection.Count; x++)
        {
            if (pickupCollection[x].activeInHierarchy)
            {
                pickupCollection[x].SetActive(false);
            }
        }

        blockColour = PickNewColour();
        RedrawGrid();
    }

    public void RedrawGrid()
    {
        StartCoroutine(SpawnInitialGrid());
    }

    IEnumerator SpawnInitialGrid()
    {
        yield return blockCollection != null;

        int initialRowsToSpawn = (int)((Camera.main.orthographicSize * 2) / blockSize);
        for (int y = 0; y < initialRowsToSpawn; y++)
        {
            for (int x = 0; x < spritesPerRow; x++)
            {
                int rnd = Random.Range(0, spawnRandomUpperLimit);

                if (rnd == 0)
                {
                    GameObject _blk = GetNewBlock();

                    if (_blk != null)
                    {
                        Vector2 pos = new Vector2((-screenWidth + (blockSize / 2)) + (blockSize * x), spawnDistY + yPos);

                        _blk.transform.position = pos;
                        _blk.transform.rotation = Quaternion.identity;
                        _blk.SetActive(true);
                    }

                   

                    //for (int i = 0; i < blockCollection.Count; i++)
                    //{
                    //    if (!blockCollection[i].activeInHierarchy)
                    //    {
                    //        //float blkScale = ResizeSprite();
                    //        //blockCollection[i].transform.localScale = new Vector3(blkScale, blkScale, 1.0f);
                    //        //Vector2 pos = new Vector2((-screenWidth + (blockSize / 2)) + (blockSize * x), spawnDistY + (blockSize * rowsSpawned));
                    //        Vector2 pos = new Vector2((-screenWidth + (blockSize / 2)) + (blockSize * x), spawnDistY + yPos);
                    //        blockCollection[i].transform.position = pos;
                    //        blockCollection[i].transform.rotation = Quaternion.identity;
                    //        blockCollection[i].SetActive(true);
                    //        break;
                    //    }
                    //}
                }

            }

            rowsSpawned++;
            yPos += blockSize;
        }
        
    }

    GameObject GetNewBlock()
    {
        //check for existing inactive block
        for (int i = 0; i < blockCollection.Count; i++)
        {
            if (!blockCollection[i].activeInHierarchy)
            {
                return blockCollection[i];
            }
        }
        //instantiate new block if required
        GameObject obj = (GameObject)Instantiate(block);
        obj.SetActive(false);
        blockCollection.Add(obj);

        return obj;
    }

    GameObject GetNewPickup()
    {
        for (int i = 0; i < pickupCollection.Count; i++)
        {
            if(!pickupCollection[i].activeInHierarchy)
            {
                return pickupCollection[i];
            }
        }

        GameObject _pu = (GameObject)Instantiate(pickUp);
        _pu.SetActive(false);
        pickupCollection.Add(_pu);
        return _pu;

    }

    public void SpawnRow(int rowsToSpawn)
    {

        for (int r = 0; r < rowsToSpawn; r++)
        {
            for (int x = 0; x < spritesPerRow; x++)
            {
                int rnd = Random.Range(0, spawnRandomUpperLimit);

                if (rnd == 0)
                {
                    GameObject _blk = GetNewBlock();
                    if (_blk != null)
                    {
                        Vector2 pos = new Vector2((-screenWidth + (blockSize / 2)) + (blockSize * x), spawnDistY + yPos);

                        _blk.transform.position = pos;
                        _blk.transform.rotation = Quaternion.identity;
                        _blk.SetActive(true);
                    }

                }
                else if (availablePickups > 0)
                {
                    int rndPu = Random.Range(0, 10);
                    if (rndPu == 0)
                    {
                        GameObject _pu = GetNewPickup();

                        if(_pu != null)
                        {
                            Vector2 puPos = new Vector2((-screenWidth + (blockSize / 2)) + (blockSize * x), spawnDistY + yPos);

                            PickUp puScript = _pu.GetComponent<PickUp>();
                            if (puScript == null)
                            {
                                Debug.LogError("No PickUp class found on Pickup object");
                            }
                            puScript.MyType = EnumManager.PickupTypes.Crystal;
                            puScript.PickupScoreBonus = (int)Random.Range(100, 500);

                            _pu.transform.position = puPos;
                            _pu.transform.rotation = Quaternion.AngleAxis(Random.Range(0.0f, 360.0f), Vector3.forward);
                            _pu.transform.localScale = spawnObjectScale;
                            _pu.SetActive(true);
                            availablePickups--;
                            if(availablePickups < 0)
                            {
                                availablePickups = 0;
                            }
                        }
                        

                    }

                }
            }

            rowsSpawned++;
            yPos += blockSize;

            if (rowsSpawned >= rowsUntilNextLevel)
            {
                rowsSpawned = 0;
                blockColour = PickNewColour();
                GameManager._current.UpdateSpeedMultiplyer();
                if (spawnRandomUpperLimit > 2)
                {
                    spawnRandomUpperLimit--;
                }

                //if (spritesPerRow < maxBlocksPerRow)
                //{
                //    spritesPerRow++;
                //    if (spriteLevel < (maxBlocksPerRow - spritesPerRow))
                //    {
                //        spriteLevel++;
                //        blockColour = PickNewColour();
                //    }

                //}

            }
        }

        

        
    }

    void SetBlockSize()
    {
        SpriteRenderer rnd = block.GetComponent<SpriteRenderer>() as SpriteRenderer;
        rnd.color = blockColour;

        //float worldScreenWidth = (Camera.main.orthographicSize * 2) / Screen.height * Screen.width;
        //float newScale = (worldScreenWidth / rnd.sprite.bounds.size.x) / spritesPerRow;

        //Vector3 _scale = new Vector3(newScale, newScale, 1.0f);
        //block.transform.localScale = _scale;

        Vector3 _scale = PersistentDataManager.Current.SetBlockSize();

        //try
        //{
        //     _scale = PersistentDataManager.Current.SessionBlockScale;
        //}
        //catch
        //{
        //    _scale = PersistentDataManager.Current.SetBlockSize();
        //}


        block.transform.localScale = _scale;

        //pickup scale
        //_scale.z = newScale;
        _scale.z = _scale.x;
        pickUp.transform.localScale = _scale;
        spawnObjectScale = _scale;
        //pickUp.transform.GetChild(0).transform.localScale = _scale;
    }

    //void CreateNewSprite()
    //{
    //    SpriteRenderer rnd = block.GetComponent<SpriteRenderer>() as SpriteRenderer;
    //    rnd.color = blockColour;

    //    //float newSpriteWidth = ((Camera.main.orthographicSize * Camera.main.aspect) * 2) / initialSpriteWidth;
    //    //Sprite newSprite = Sprite.Create(masterTexture, new Rect(0.0f, 0.0f, newSpriteWidth * 100, newSpriteWidth * 100), new Vector2(0.5f, 0.5f), 100);
    //    //initialSpriteWidth = (int)((Camera.main.orthographicSize * Camera.main.aspect) * 2) / 10;
    //    initialSpriteWidth = Screen.width / 10;
        
    //    //NOPE!
    //    masterTexture.Resize(initialSpriteWidth, initialSpriteWidth, TextureFormat.RGBA32, false);
    //    masterTexture.Apply();
    //    Sprite newSprite = Sprite.Create(masterTexture, new Rect(0.0f, 0.0f, initialSpriteWidth, initialSpriteWidth), new Vector2(0.5f, 0.5f), 100.0f);

    //    rnd.sprite = newSprite;

    //    //rnd.sprite = newBLock;

    //    BoxCollider2D col = block.GetComponent<BoxCollider2D>();
    //    if (col == null)
    //    {
    //        col = block.AddComponent<BoxCollider2D>();
    //        block.GetComponent<BoxCollider2D>().isTrigger = true;
    //    }

    //    col.size = rnd.bounds.size;
    //}

    //float ResizeSprite()
    //{
    //    return 10.0f / spritesPerRow;
    //}
    

    Color PickNewColour()
    {
        Color newCol = new Color(Random.Range(0.3f, 0.8f), Random.Range(0.3f, 0.8f), Random.Range(0.3f, 0.8f), 1.0f);

        //if((newCol.r < 0.5f && newCol.g < 0.5f && newCol.b < 0.5f) || newCol == blockColour)
        if (newCol == blockColour)
        {
            newCol = new Color(Random.Range(0.4f, 0.6f), Random.Range(0.4f, 0.6f), Random.Range(0.4f, 0.6f), 1.0f);
        }
        
        return newCol;
        //int rnd = Random.Range(0, blockColours.Length);
        //if(blockColour == blockColours[rnd])
        //{
        //    rnd = rnd++;
        //    if(rnd > blockColours.Length - 1)
        //    {
        //        rnd = 0;
        //    }
        //}
        //switch (rnd)
        //{
        //    case 0:
        //        return blockColours[0];
        //    case 1:
        //        return blockColours[1];
        //    case 2:
        //        return blockColours[2];
        //    case 3:
        //        return blockColours[3];
        //    default:
        //        return blockColours[3];
        //}

    }

    public void SpawnPlayerDeathEffect(Vector3 pos)
    {
        if(deathEffect == null)
        {
            deathEffect = Instantiate(Resources.Load("DeathEffect", typeof(PlayerExplosion)), Vector3.right * 20, Quaternion.identity) as PlayerExplosion;
        }

        deathEffect.ShowEffect(pos);
    }

    public void SpawnPopupText(Vector3 pos, int amt)
    {
        for (int i = 0; i < popupTextCollection.Count; i++)
        {
            if(!popupTextCollection[i].gameObject.activeInHierarchy)
            {
                popupTextCollection[i].transform.position = pos;                
                popupTextCollection[i].gameObject.SetActive(true);
                popupTextCollection[i].Show(amt);
                return;
            }
        }

        //instantiate new popupText if none available
        PopupText _pt = Instantiate(Resources.Load("PopupText", typeof(PopupText)) as PopupText);
        popupTextCollection.Add(_pt);
        _pt.Show(amt);
    }
}
