﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioEffects { TARGET_MOVE, COLLECT_PICKUP, DEATH, WARP }

public class AudioManager : MonoBehaviour
{
    public static AudioManager curAudioManager { get; private set; }
    [SerializeField]AudioClip a_targetMove, a_collectPickup, a_death, a_warp;
    AudioSource audioSource;

    private void Awake()
    {
        if(curAudioManager != null && curAudioManager != this)
        {
            Destroy(this);
        }
        else
        {
            curAudioManager = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if(a_collectPickup == null)
        {
            a_collectPickup = (AudioClip)Resources.Load("Audio/CollectPickup");
        }
        if (a_death == null)
        {
            a_death = (AudioClip)Resources.Load("Audio/DeathAudio");
        }
        if (a_targetMove == null)
        {
            a_targetMove = (AudioClip)Resources.Load("Audio/TargetMove");
        }
        if (a_warp == null)
        {
            a_warp = (AudioClip)Resources.Load("Audio/PlayerWarp");
        }
        if(audioSource == null)
        {
            audioSource = gameObject.GetComponent<AudioSource>();
        }

        UpdateVolume();
    }

    public void PlayEffect(AudioEffects effectToPlay)
    {
        audioSource.loop = effectToPlay == AudioEffects.TARGET_MOVE? true : false;

        switch (effectToPlay)
        {
            case AudioEffects.TARGET_MOVE:
                audioSource.PlayOneShot(a_targetMove);
                break;
            case AudioEffects.COLLECT_PICKUP:
                audioSource.PlayOneShot(a_collectPickup);
                break;
            case AudioEffects.DEATH:
                StopAudio();
                audioSource.PlayOneShot(a_death);
                break;
            case AudioEffects.WARP:
                StopAudio();
                audioSource.PlayOneShot(a_warp);
                break;
            default:
                break;
        }
    }

    void StopAudio()
    {
        audioSource.Stop();
    }

    void UpdateVolume()
    {
        audioSource.volume = PersistentDataManager.Current.AudioVolume;
    }
}
