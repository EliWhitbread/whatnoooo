﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerSettings : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI difficultyDisplayText;
    [SerializeField] Slider difficultySlider, volumeSlider;
    [SerializeField] GameObject settingsPanel, gdrpPanel, privacyPanel, policiesPanel;
    [SerializeField] int userDifficulty;
    float userVolume;
    PlayerSaveData tmpData;

    private void Start()
    {
        gdrpPanel.SetActive(false);
        privacyPanel.SetActive(false);
        policiesPanel.SetActive(false);

        difficultySlider.onValueChanged.AddListener(delegate { UpdateDiffuculty(); });
        volumeSlider.onValueChanged.AddListener(delegate { UpdateVolume(); });

        tmpData = JsonData.instance.LoadSavaData();

        //GDRP
        if(tmpData.userViewedGDRP == false)
        {
            OpenPrivacySettings();
        }

        userDifficulty = tmpData.difficulty;
        PersistentDataManager.Current.blocksPerRow = 3 + userDifficulty;
        userVolume = tmpData.volume;
        PersistentDataManager.Current.AudioVolume = userVolume;
    }


    void UpdateDiffuculty()
    {
        userDifficulty = (int)difficultySlider.value;
        difficultyDisplayText.text = userDifficulty.ToString();
        tmpData.difficulty = userDifficulty;
        JsonData.instance.SavePlayerData(tmpData);
        PersistentDataManager.Current.blocksPerRow = 3 + userDifficulty;
    }

    void UpdateVolume()
    {
        userVolume = volumeSlider.value;
        tmpData.volume = userVolume;
        JsonData.instance.SavePlayerData(tmpData);
        PersistentDataManager.Current.AudioVolume = userVolume;
    }

    public void ToggleSettingsMenu()
    {
        bool active = !settingsPanel.activeSelf;

        settingsPanel.SetActive(active);

        if (active == true)
        {
            tmpData = JsonData.instance.LoadSavaData();
            userDifficulty = tmpData.difficulty;
            difficultySlider.value = (float)userDifficulty;
            difficultyDisplayText.text = userDifficulty.ToString();
            userVolume = tmpData.volume;
            volumeSlider.value = userVolume;
        }
    }

    public void OpenPrivacySettings()
    {
        gdrpPanel.SetActive(true);
        privacyPanel.SetActive(true);
    }

    public void GdrpOptions(int selection)
    {
        switch (selection)
        {
            case 0: //view policies
                policiesPanel.SetActive(true);
                break;
            case 1:
                tmpData.userViewedGDRP = true;
                tmpData.gdrpUserDataConsentGiven = true;
                gdrpPanel.SetActive(false);
                privacyPanel.SetActive(false);
                policiesPanel.SetActive(false);
                Debug.LogError("GDPR consent given");
                break;
            case 2:
                tmpData.userViewedGDRP = true;
                tmpData.gdrpUserDataConsentGiven = false;
                gdrpPanel.SetActive(false);
                privacyPanel.SetActive(false);
                policiesPanel.SetActive(false);
                Debug.LogError("GDPR consent revoked");
                break;
            case 3: //back button
                policiesPanel.SetActive(false);
                break;
            default:
                break;
        }

        JsonData.instance.SavePlayerData(tmpData);

    }

    public void ShowPrivacyPolicies(int selection)
    {
        switch (selection)
        {
            case 0:
                Application.OpenURL("https://policies.google.com/privacy");
                break;
            case 1:
                Application.OpenURL("https://drive.google.com/file/d/1g_qygZjUFzvfKGHv8rBJ-I6E_sP8IKVh/view?usp=sharing");
                break;
            default:
                break;
        }
    }
}
