﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class JsonData : MonoBehaviour
{
    public static JsonData instance { get; private set; }
    PlayerSaveData saveData;
    string dataPath;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }

        GetDataPath();
    }

    //private void Start()
    //{
    //    GetDataPath();
    //}

    void GetDataPath()
    {
        dataPath = Path.Combine(Application.persistentDataPath, "PlayerData.txt");
    }

    public void SavePlayerData(PlayerSaveData saveData)
    {
        //if (dataPath == null)
        //{
        //    GetDataPath();
        //}

        string jsonString = JsonUtility.ToJson(saveData);
        using (StreamWriter streamWriter = File.CreateText(dataPath))
        {
            streamWriter.Write(jsonString);
        }
    }

    public PlayerSaveData LoadSavaData()
    {
        if(File.Exists(dataPath))
        {
            using (StreamReader streamReader = File.OpenText(dataPath))
            {
                string jsonString = streamReader.ReadToEnd();
                return JsonUtility.FromJson<PlayerSaveData>(jsonString);
            }
        }
        else
        {
            PlayerSaveData tmpData = new PlayerSaveData
            {
                bestScore = 0,
                bestTime = 0.0f,
                crystals = 0,
                volume = 0.6f,
                difficulty = 4,
                gdrpUserDataConsentGiven = false,
                userViewedGDRP = false

            };
            SavePlayerData(tmpData);
            return tmpData;
        }
        
    }
    
    public void ClearSavedData(bool clearAll = false)
    {
        if (File.Exists(dataPath))
        {
            PlayerSaveData saveData = LoadSavaData();

            saveData.bestScore = 0;
            saveData.bestTime = 0.0f;
            saveData.crystals = clearAll == true ? 0 : saveData.crystals;
            saveData.volume = 0.6f;
            saveData.difficulty = 4;

            SavePlayerData(saveData);
        }
    }

}

[System.Serializable]
public struct PlayerSaveData
{
    public int bestScore, crystals, difficulty;
    public float bestTime, volume;
    public bool userViewedGDRP, gdrpUserDataConsentGiven;
}