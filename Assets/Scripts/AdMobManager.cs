﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;


public class AdMobManager : MonoBehaviour
{
    bool playerWatchedAd = false;

    //adMod ids
#if UNITY_ANDROID
    string appId = "ca-app-pub-7215526310118950~4928395455";
    string adUnitId = "ca-app-pub-7215526310118950/9546297319";
    //string adUnitId = "ca-app-pub-3940256099942544/5224354917"; //test unityId

#elif UNITY_IOS
    string appId = "ca-app-pub-7215526310118950~7981455277";
    string adUnitId = "ca-app-pub-7215526310118950/8033426165";
    //string adUnitId = "ca-app-pub-3940256099942544/1712485313"; //test unityId

#else
    string appId = "unexpected_platform";
    string adUnitId = "unexpected_platform";

#endif

    RewardBasedVideoAd rewardBasedVideoAd;
    bool userDataConsentGiven = false;

    private void Start()
    {
        Debug.Log("Test Ads in use!");

        //EU GDPR Check
        PlayerSaveData _data = JsonData.instance.LoadSavaData();
        userDataConsentGiven = _data.gdrpUserDataConsentGiven;

        MobileAds.Initialize(appId);

        this.rewardBasedVideoAd = RewardBasedVideoAd.Instance;

        //rewardBasedVideoAd.OnAdLoaded += RewardBasedVideoAd_OnAdLoaded;
        rewardBasedVideoAd.OnAdFailedToLoad += RewardBasedVideoAd_OnAdFailedToLoad;
        rewardBasedVideoAd.OnAdRewarded += RewardBasedVideoAd_OnAdRewarded;
        rewardBasedVideoAd.OnAdClosed += RewardBasedVideoAd_OnAdClosed;

        this.RequestRewardVideo();
    }

    private void OnDestroy()
    {
        //rewardBasedVideoAd.OnAdLoaded -= RewardBasedVideoAd_OnAdLoaded;
        rewardBasedVideoAd.OnAdFailedToLoad -= RewardBasedVideoAd_OnAdFailedToLoad;
        rewardBasedVideoAd.OnAdRewarded -= RewardBasedVideoAd_OnAdRewarded;
        rewardBasedVideoAd.OnAdClosed -= RewardBasedVideoAd_OnAdClosed;
    }

    public void ShowRewardAd()
    {
        if(rewardBasedVideoAd.IsLoaded())
        {
            rewardBasedVideoAd.Show();
        }
        else
        {
            //Debug.Log("Ad not ready yet!");
            StartCoroutine(ShowAd());
        }
    }

    IEnumerator ShowAd()
    {
        bool ready = rewardBasedVideoAd.IsLoaded();
        float _t = 3.0f;
        while (_t > 0.0f || ready == false)
        {
            _t -= Time.deltaTime;
            ready = rewardBasedVideoAd.IsLoaded();
            yield return null;
        }

        if(ready == true)
        {
            rewardBasedVideoAd.Show();
        }
        else
        {
            Debug.LogError("Ad will not load!");
        }
        
    }

    private void RewardBasedVideoAd_OnAdClosed(object sender, System.EventArgs e)
    {
        GameManager._current.ContinueAfterAd(playerWatchedAd);

        this.RequestRewardVideo();
        //Debug.Log("Ad closed" + e.ToString());
    }

    private void RewardBasedVideoAd_OnAdRewarded(object sender, Reward e)
    {
        //Debug.Log("Ad reward given" + e.Type.ToString() + " " + e.Amount.ToString());
        playerWatchedAd = true;
    }

    private void RewardBasedVideoAd_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        //Debug.Log("Ad FAILED to load" + e.Message);
        GameManager._current.ContinueAfterAd(false);
    }

    //private void RewardBasedVideoAd_OnAdLoaded(object sender, System.EventArgs e)
    //{
    //    Debug.Log("Ad LOADED" + e.ToString());
    //}

    void RequestRewardVideo()
    {
        playerWatchedAd = false;

        if(userDataConsentGiven == true)
        {
            AdRequest request = new AdRequest.Builder().Build();
            this.rewardBasedVideoAd.LoadAd(request, adUnitId);
        }
        else
        {
            AdRequest request = new AdRequest.Builder().AddExtra("npa", "1").Build();
            this.rewardBasedVideoAd.LoadAd(request, adUnitId);
        }
        
    }
}
